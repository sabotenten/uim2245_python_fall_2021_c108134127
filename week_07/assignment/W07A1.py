import sys
def qaq(S):
    ans = [] #多種答案list
    a_sum = 0 # a的ascii總和 a是當前回文
    result_sum = 0 # ascii總和
    result = "" #輸出一個答案
    x = 0 
    y = len(input)-1
    # print (x,y)
    if x+1 == y and S[x] == S[y]:
        return S
    for i in range (x,y):  # 0~len 
        for j in range(y,i+1,-1): #len~i
            # print("S[i]=",S[i]," S[j]=",S[j])
            i1 = i
            j1 = j #以防動到i,j值
            if S[i] == S[j]:
                # print(S[i],S[j])
                while(i1 != j1 or i1+1 != j1):
                    i1=i1+1
                    j1=j1-1
                    # print(S[i1],S[j1])
                    if S[i1]!= S[j1]:
                        # print('不是回文')
                        break
                    if i1 == j1 or i1+1 == j1:
                        # print(S[i:j+1])
                        a = S[i:j+1]      
                        ans.append(a)
                        if len(a) > len(result):
                            result = a
                        elif len(a) == len (result):
                            for char in a:
                                a_sum = a_sum + int(ord(char))
                            # print(a_sum)
                            for char in result:
                                result_sum = result_sum + int(ord(char))
                            # print(result_sum)
                            if a_sum <= result_sum:
                                result = a
                        break
    print(ans)
    return result

if __name__ == '__main__':
    input = str (sys.argv[1])
    # print(input)
    print(qaq(input)) #2 ~ n+2

'''
input[x:y]
    x = 0 
    y = len(input)-1
從第一個字開始比對 (從最後一個一個比回來 比到x+2的時候換下一輪)
    if  遇到一樣的
        往下一個數字 比對 一樣的往前一個 (就是往中間比)
        if 中間遇到不一樣的 > 就不是回文
        一直比到 i1 == j1 or i1+1 == j1 都沒有跳出的話 就是回文
'''


