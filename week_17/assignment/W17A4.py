import pandas as pd
import matplotlib.pyplot as plt

'''
根據「train.csv」計算艙等與活下來的關係（總共需要有六條）
'''

def DFTicketClassToSurvived():
    df = pd.read_csv('./train.csv')
    df.groupby(['Pclass','Survived'])['PassengerId'].count().plot(kind = 'bar')
    
    plt.xticks(rotation=0)
    plt.savefig('ticketclass_to_survived.png')

DFTicketClassToSurvived()