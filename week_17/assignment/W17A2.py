import pandas as pd
import matplotlib.pyplot as plt

'''
基於train.csv中的性別與生還，進行乘客數量統計，並繪製成直方圖（不需計算比例）
'''

def DFHistogram():
    
    df = pd.read_csv('./train.csv')
    # print(df.groupby(['Sex'])['PassengerId'].count())
    # print(df.groupby(['Sex','Survived'])['PassengerId'].count())

    df.groupby(['Sex','Survived'])['PassengerId'].count().plot(kind = 'bar')
    plt.xticks(rotation=0)
    # plt.show()
    
    plt.savefig('Histogram.png')
    
DFHistogram()