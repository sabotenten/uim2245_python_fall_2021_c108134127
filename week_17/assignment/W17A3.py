import pandas as pd
import matplotlib.pyplot as plt

'''
根據「train.csv」以圖的方式計算男女生還比例，並敘述其關係
'''

def DFSurvived():
    
    df = pd.read_csv('./train.csv')
    
    df.groupby(['Sex','Survived'])['PassengerId'].count().plot(legend=None , title = 'Survived' , kind = 'pie' , autopct = '%.2f%%')
          
    plt.savefig('survived.png')
    
DFSurvived()