import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('./train.csv')
# print(df)

'''
print(df.info())
Cabin 客艙編號 資料太少 直接去掉
Age 少了約150筆資料 需要補上
Embarked 登港 缺2人 不用管
'''
# print(df.describe())
# print(df.isnull()) #檢查空值
# print(df.isnull().sum()) #統計空值
# print(df[df['Age'].isnull() == True]) # print出Age是空的

x = df['Age'].mean() #用平均補
df['Age'] = df['Age'].fillna(x)

'''
補齊上船地點空值
各地點出現次數
S    644
C    168
Q     77
'''
# print(df['Embarked'].value_counts())
df['Embarked'] = df['Embarked'].fillna('S')


df = df.drop('Cabin', axis= 1) #axis = 1 代表刪除欄位
# print(df.isnull().sum())

df.to_csv('train_new.csv')