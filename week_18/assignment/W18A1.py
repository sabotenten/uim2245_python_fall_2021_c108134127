import pandas as pd
import matplotlib.pyplot as plt

'''
請根據「BMI-5_9.csv」，繪製出生年與BMI的散佈圖繳交
 W18A1.py & w18a1.png
'''

df = pd.read_csv('./BMI-5_9.csv')

plt.scatter('Period', 'Mean BMI', data=df, marker='*', alpha = 0.2 )

plt.savefig('w18a1.png')