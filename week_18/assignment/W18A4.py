 # loading data

import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

i_filepath = './'

df_train = pd.read_csv("train.csv")
# df_test = pd.read_csv("test.csv")   # 這裡各位可以忽略，這是Kaggle比賽用
# df_data = df_train.append(df_test)  # 這裡各位可以忽略，這是Kaggle比賽用 
df = df_train
# df.info()

df['Age']=df['Age'].fillna(df['Age'].mean()) 
df['Embarked']=df['Embarked'].fillna('S') 
df=df.drop('Cabin', axis=1) 
# print('重複值：', df[df.duplicated()]) #檢查有無重複值
df['Sex']=df['Sex'].map({'female':0, 'male':1}) 
df['Embarked']=df['Embarked'].map({'S':0, 'C':1, 'Q':2}) 
# df.info()

df_X = df[['Sex','Pclass']]
df_y = df['Survived']


X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size = 0.2 )

k = 1
knn=KNeighborsClassifier(n_neighbors=k) 
knn.fit(X_train, y_train) 

print('----KNN模式訓練後，取test data 進行分類的準確率計算-------')
print('\n')
print('準確率:',knn.score(X_test,y_test))
print('\n')


for i in range(3,11):
    k=i
    knn=KNeighborsClassifier(n_neighbors=k)  
    knn.fit(X_train, y_train)  # 用 training data 去訓練模型
    print('k =',k,' 準確率:',knn.score(X_test,y_test))  #用 test data 檢測模型的準確率
    
    
k = 5
knn=KNeighborsClassifier(n_neighbors=k)  
knn.fit(X_train, y_train)


print('分類的預測結果：')
pred = knn.predict(X_test) 
print(pred) #觀察預測結果
print('\n')
print('真實數據：')
print(y_test.values)  #觀察真實數據(Test data)
print('\n')

pred = knn.predict(X_test) 
accuracy_score(y_test, pred)

confusion_matrix(y_test, pred)


from sklearn.model_selection import cross_val_score
s=cross_val_score(knn, df_X, df_y, scoring='accuracy', cv=10)
print('準確率：',s)
print('平均準確率：',s.mean())
print('最高：',s.max())
print('最差：',s.min())
print('\n')

print(' ----------------電影中兩位主角的生還推測-------------')
print('\n')
Rose=[[0,1]] #女性 頭等艙 蘿絲（Rose DeWitt Bukater）
Jack=[[1,3]] #男性 三等艙 傑克（Jack Dawson）
v=knn.predict(Rose)
if v==1:
    s='生還'
else:
    s='死亡'
print('Rose能生還嗎 ? ', s)           #Rose為女性,及坐頭等艙

v=knn.predict(Jack)
if v==1:
    s='生還'
else:
    s='死亡'
print('Jack能生還嗎 ? ', s)           #Jack為男性,及坐三等艙



'''
加入其他特徵
'''
print('\n')
print('==================================加入其他特徵後，最差準確率提高=====================================')
print('\n')

df_X = df[['Sex','Pclass','Age']]
df_y = df['Survived']


X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size = 0.2 )

k = 1
knn=KNeighborsClassifier(n_neighbors=k) 
knn.fit(X_train, y_train) 

print('----KNN模式訓練後，取test data 進行分類的準確率計算-------')
print('\n')
print('準確率:',knn.score(X_test,y_test))
print('\n')


for i in range(3,11):
    k=i
    knn=KNeighborsClassifier(n_neighbors=k)  
    knn.fit(X_train, y_train)  # 用 training data 去訓練模型
    print('k =',k,' 準確率:',knn.score(X_test,y_test))  #用 test data 檢測模型的準確率
    
    
k = 5
knn=KNeighborsClassifier(n_neighbors=k)  
knn.fit(X_train, y_train)


print('分類的預測結果：')
pred = knn.predict(X_test) 
print(pred) #觀察預測結果
print('真實數據：')
print(y_test.values)  #觀察真實數據(Test data)


pred = knn.predict(X_test) 
accuracy_score(y_test, pred)

confusion_matrix(y_test, pred)


from sklearn.model_selection import cross_val_score
s=cross_val_score(knn, df_X, df_y, scoring='accuracy', cv=10)
print('準確率：',s)
print('平均準確率：',s.mean())
print('最高：',s.max())
print('最差：',s.min())
print('\n')

print(' ----------------電影中兩位主角的生還推測-------------')
print('\n')
Rose=[[0,1 , 17]] #女性 頭等艙 蘿絲（Rose DeWitt Bukater）
Jack=[[1,3 , 20]] #男性 三等艙 傑克（Jack Dawson）
v=knn.predict(Rose)
if v==1:
    s='生還'
else:
    s='死亡'
print('Rose能生還嗎 ? ', s)           #Rose為女性,及坐頭等艙

v=knn.predict(Jack)
if v==1:
    s='生還'
else:
    s='死亡'
print('Jack能生還嗎 ? ', s)           #Jack為男性,及坐三等艙