def jump(x):
    if x == 1:
        return 1
    elif x == 2:
        return 2
    else:
        return jump(x-1)+(x-2)

num = int (input())
print(jump(num))


 #先跳一步或先跳兩步 剩下的步數就是前面的解
 #ex: 5階 = 跳1步 + 4階的解 
 #    5階 = 跳2步 + 3階的解 ('2'+'21') or ('2'+'12') 組成一種新組合 但其實組合數是一樣的
