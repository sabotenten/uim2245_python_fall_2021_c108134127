import pandas as pd
import matplotlib.pyplot as plt

def DFYearTotal():
    df_year = pd.read_csv('年度銷售金額.csv')
    df_year.index = df_year['AREA']
    df_year = df_year.drop('AREA' , axis = 1)
    df_year[''] = df_year['1st'] + df_year['2nd'] + df_year['3rd'] + df_year['4th']
    # print(df_year)
    df_year[''].plot(title = 'year total' , kind = 'pie' , autopct = '%.2f%%', colors = ['#5584AC','#406882','#6998AB','#B1D0E0'])
    plt.savefig('W16A1.png') 
    
DFYearTotal()