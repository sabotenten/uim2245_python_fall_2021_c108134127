import pandas as pd
import matplotlib.pyplot as plt
from pandas.core.frame import DataFrame


def DFPenguin():
    df = pd.read_csv('penguin.csv')
    df.index = df['ID']
    df = df.drop('ID' , axis = 1)
        
    
    df_gentoo = df.loc[df['Species'] == 'Gentoo penguin']
    df_gala = df.loc[df['Species'] == 'Galapagos penguin']
    df_chin = df.loc[df['Species'] == 'Chinstrap penguin']
    df_litt  = df.loc[df['Species'] == 'Little penguin']
    
    df_gentoo_re = DataFrame()
    df_gala_re = DataFrame()
    df_chin_re = DataFrame()
    df_litt_re = DataFrame()
    
    
    df_gentoo_re[['Length_cm' , 'Weight_g'] ] = df_gentoo[['Length_cm', 'Weight_g']]
    df_gala_re[['Length_cm' , 'Weight_g'] ] = df_gala[['Length_cm', 'Weight_g']]
    df_chin_re[['Length_cm' , 'Weight_g'] ] = df_chin[['Length_cm', 'Weight_g']]
    df_litt_re[['Length_cm' , 'Weight_g'] ] = df_litt[['Length_cm', 'Weight_g']]
    
    df_gentoo_re['Weight_kg'] = df_gentoo_re['Weight_g'] / 1000
    df_gala_re['Weight_kg'] = df_gala_re['Weight_g'] / 1000
    df_chin_re['Weight_kg'] = df_chin_re['Weight_g'] / 1000
    df_litt_re['Weight_kg'] = df_litt_re['Weight_g'] / 1000
    
    plt.scatter('Length_cm', 'Weight_kg', data=df_gentoo_re, marker='x', alpha = 0.2 , label='Gentoo')
    plt.scatter('Length_cm', 'Weight_kg', data=df_gala_re, marker='*', alpha = 0.2 , label='Galapagos')
    plt.scatter('Length_cm', 'Weight_kg', data=df_chin_re, alpha = 0.2, label = 'Chinstrap')
    plt.scatter('Length_cm', 'Weight_kg', data=df_litt_re, alpha = 0.2 , label = 'Little')
    plt.legend(loc = 'upper left')
    plt.title('Length(cm) & Weight(kg)')
    
    plt.savefig('W16A4.png')


DFPenguin()

