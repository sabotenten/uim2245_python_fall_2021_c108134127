import pandas as pd
import matplotlib.pyplot as plt


def DFPenguin():
    df = pd.read_csv('penguin.csv')
    df.index = df['ID']
    df = df.drop('ID' , axis = 1)

    df_In = pd.DataFrame()
    df_In['fq'] = df[['Inspector']].value_counts()
    df_In = df_In.sort_values('fq', ascending = False)
    df_In['rank'] = list(range(1,len(df_In)+1))
    df_In = df_In.reset_index()

    print(df_In['Inspector'].head(3))

DFPenguin()