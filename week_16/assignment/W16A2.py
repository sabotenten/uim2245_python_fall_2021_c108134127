import pandas as pd
import matplotlib.pyplot as plt
from pandas.core.frame import DataFrame


def DFPenguin():
    df = pd.read_csv('penguin.csv')
    df.index = df['ID']
    df = df.drop('ID' , axis = 1)
    df_sp = DataFrame()
    df_sp[''] = df['Species'].value_counts()
    print(df_sp)
    df_sp.plot(legend=None , title = 'Species',  y = '' ,kind = 'pie',autopct = '%.2f%%' , colors = ['#5584AC','#406882','#6998AB','#B1D0E0'])
    plt.savefig('W16A2.png') 

DFPenguin()