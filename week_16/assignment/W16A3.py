import pandas as pd
import matplotlib.pyplot as plt

'''
請根據每個品種去計算平均身高及體重，並將這些圖進行比較不同品種之間的身高與體重差異
針對各個品種顯示其平均身高體重(圖片)，根據各品種顯示其身高體重進行比較（文字)
輸出結果 : 直方圖 
一張將圖片儲存,文字儲存
'''
def DFPenguin():
    
    df = pd.read_csv('penguin.csv')
    df.index = df['ID']
    df = df.drop('ID' , axis = 1)

    df_gentoo = df.loc[df['Species'] == 'Gentoo penguin']
    df_gala = df.loc[df['Species'] == 'Galapagos penguin']
    df_chin = df.loc[df['Species'] == 'Chinstrap penguin']
    df_litt = df.loc[df['Species'] == 'Little penguin']

    df_gen_result = df_gentoo[['Length_cm','Weight_g']].mean()
    df_gala_result = df_gala[['Length_cm','Weight_g']].mean()
    df_chin_result = df_chin[['Length_cm','Weight_g']].mean()
    df_litt_result = df_litt[['Length_cm','Weight_g']].mean()


    df_result = pd.DataFrame({'Gentoo penguin':df_gen_result, 'Galapagos penguin':df_gala_result,'Chinstrap penguin':df_chin_result ,'Little penguin':df_litt_result })


    df_result = df_result.T

    df_result['Weight_kg'] = df_result['Weight_g'] / 1000
    df_result = df_result.drop('Weight_g' , axis = 1)
    df_result.plot(kind = 'bar')
    plt.xticks(rotation=10)
    plt.savefig('W16A3.png') 
        
DFPenguin()