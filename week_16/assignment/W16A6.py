import pandas as pd
import matplotlib.pyplot as plt
from pandas.core.frame import DataFrame


def DFPenguin():

    df = pd.read_csv('penguin.csv')
    df.index = df['ID']
    df = df.drop('ID' , axis = 1)
    df_re2 = pd.DataFrame([1,2,3,4,5,6,7,8,9,10,11,12],index = ['1' , '2', '3' ,'4' ,'5' ,'6' ,'7' ,'8' ,'9' ,'10' ,'11' ,'12'] ,columns = ['M'])
    dr_re = pd.DataFrame([1,2,3,4,5,6,7,8,9,10,11,12],index = ['1' , '2', '3' ,'4' ,'5' ,'6' ,'7' ,'8' ,'9' ,'10' ,'11' ,'12'] ,columns = ['M'])
    df_re2['fq'] = df["Measurement Date"].str.split("/").str.get(1).value_counts()
    df_re = df_re2.merge(df_re2, how='outer')
    df_re['fq'] = df_re['fq'].fillna(0)
    # print(df_re)
    df_re.plot(kind = 'bar' , x = 'M' , y = 'fq' , title = 'month')
    plt.xticks(rotation=0)
    plt.savefig('W16A6.png')
    
DFPenguin()