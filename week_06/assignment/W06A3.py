#階乘
import sys
def fac(num):
    ans = 1
    for i in range(1,num):
        ans = ans * num
        num -= 1
    return ans

# n = int (input())
# print(fac(n))
if __name__ == '__main__':
    input = int (sys.argv[1])
    print(fac(input))