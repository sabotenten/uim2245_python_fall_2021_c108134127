import sys
def gcd(m,n):
    if n == 0 or m == 0:
        return None
    if m > n:
        while True:
            a = m % n
            m = n
            n = a
            if a == 0:
                return m
                break
    if n > m:
        while True:
            a = n % m
            n = m
            m = a
            if a == 0:
                return n
                break
    if n == m:
        return n

if __name__ == '__main__':
    a = int (sys.argv[1])
    b = int (sys.argv[2])

    print(gcd(a,b))