import random
import sys
from math import sqrt
def darts(q):
    x1 = random.random()*10
    y1 = random.random()*10
    # print('x1 = ',x1)
    # print('y1 = ',y1)
    count_a = 0
    count_b = 0
    for i in range (q):
        x2 = random.random()*10
        y2 = random.random()*10
        # print('x2 = ',x2)
        # print('y2 = ',y2)
        if sqrt((x2-x1)**2+(y2-y1)**2) <= 1:
            count_a = count_a + 1
        elif sqrt((x2-x1)**2+(y2-y1)**2) > 1:
            count_b = count_b + 1
    result = [count_a,count_b]
    return result

if __name__ == '__main__':
    input = int (sys.argv[1])
    print(darts(input))