import random

def mora(): #W10A1
    result = 1 # win = 0 lose = 1 平手 = 2
    while result != 0:
        computer = random.randint(0,2) # 0 = 剪刀 1 = 石 2 = 布
        player_input = input('輸入剪刀 石頭 布 : \n')
        if player_input == '剪刀':
            player = 0
        elif player_input == '石頭':
            player = 1
        else:
            player = 2
        
        if player == computer:
            result = 2
        elif player == 0:
            if computer == 1:
                result = 1
            else:
                result = 0
        elif player == 1:
            if computer == 0:
                result = 0
            else:
                result = 1
        else:
            if computer == 0:
                result = 1
            else:
                result = 0
        
        if result == 0:
            result_n = 'win'
        elif result == 1:
            result_n = 'lose'
        else:
            result_n = 'tie'

        if computer == 0:
            computer_out = '剪刀'
        elif computer == 1:
            computer_out = '石頭'
        else:
            computer_out = '布'
        print ('computer :',computer_out,'player :',player_input, 'result :',result_n)
            
#===========================================================#

def mora2(): #W10A2
    result = 1 # win = 0 lose = 1 平手 = 2
    while result != 0:
        player_input = input('輸入剪刀 石頭 布 : \n')
        if player_input == '剪刀':
            player = 0
        elif player_input == '石頭':
            player = 5
        else:
            player = 10
        computer = random.randrange(0,11,5) # 刀=0 石=5 布=10
        # 非窮舉
        if player == computer:
            result = 2
        elif player - computer == -10 or player - computer == 5:
            result = 0
        else:
            result = 1
        
        if result == 0:
            result_n = 'win'
        elif result == 1:
            result_n = 'lose'
        else:
            result_n = 'tie'

        if computer == 0:
            computer_out = '剪刀'
        elif computer == 5:
            computer_out = '石頭'
        else:
            computer_out = '布'
        print ('computer :',computer_out,'player :',player_input, 'result :',result_n)
            
#===========================================================#

def paint_simple(n,m,k): #W10A3

    A = [[0 for i in range(m)] for i in range(n)]
    num  = []
    for x in range (k):
        i = str(random.randint(0,n-1))
        j = str(random.randint(0,m-1))
        x = '('+ i + ','+ j +')'
        while x in num :
            i = str(random.randint(0,n-1))
            j = str(random.randint(0,m-1))
            x = '('+ i + ','+ j +')'
        num.append(x)
        i = int(i)
        j = int(j)
        print(num)
        A[i][j] = 1

    for i in range (n):
        for j in range (m):
            print (A[i][j],end = ' ')
        print("\n" , end = "")
    
#===========================================================#

# def paint(n,m,k): #W10A4
    
    #>> 做完檢查再回來做這個

    # A = [[0 for i in range(m)] for i in range(n)]
    # num  = []
    # #先隨機選一點 
    # i = str(random.randint(0,n-1))
    # j = str(random.randint(0,m-1))
    # x = '('+ i + ','+ j +')'
    # num.append(x)
    # i = int(i)
    # j = int(j)
    # A[i][j] = 1
    # for x in range (k-1): #從點往外

    #     num.append(x)
    #     i = int(i)
    #     j = int(j)
    #     # print(num)
    #     A[i][j] = 1

    # for i in range (n):
    #     for j in range (m):
    #         print (A[i][j],end = ' ')
    #     print("\n" , end = "")

# paint(5,5,5)

#===========================================================#

def paint_check(n):
    n_copy = copy.deepcopy(n)
    
    sum_1 = 0 
    for k in range (len(n[0])): #計算整張圖有幾個1
        sum_1 = sum_1 + n[k].count(1)
    #=====如果 1個數 = row or column 長度 再判斷=====#
    # if sum_1 == len(n[0]) or sum_1 == len(n): #判斷1是不是一整排的
    #     for i in range (len(n)-1):
    #         for j in range(len(n[0]-1)):
    #             while n_copy[j][i] != 1:
    #                 n_copy.remove([0][i])
                

    # for i in range (n):
    #     for j in range (m):
    #         print (A[i][j],end = ' ')
    # print("\n" , end = "")
    
    
#====測試用====#

# import copy
# n = [[1,1,0,1,1],[0,0,1,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
# for i in range (0,6): #column
#     for j in range (0,5): #row
#         print (n[i][j],end = ' ')
#     print("\n" , end = "")
# print(len(n)) #row
# print(len(n[0])) #column
# print(n[0][0:])
# print(n[0].count(1))
# y = copy.deepcopy(n)
# print('y = ',y)

# paint_check(n)