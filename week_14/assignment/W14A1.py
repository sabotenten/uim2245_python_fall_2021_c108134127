import pandas as pd

def DFOfGrades():
    df = pd.read_csv('./學生成績檔-4-2.8.csv')
    '''
    1. 插入總平均 名次
    '''
    s = ['1080041','葉大熊',1,'1080041@sun.tc.edu.tw',90,80,35,45,65]  
    df1 = pd.DataFrame(data = [s] ,columns = df.columns)
    df2 = df.append(df1,ignore_index = True)

    c = ['第1次平時考','第2次平時考','第3次平時考','第4次平時考','第5次平時考']
    df2['總平均'] = df2[c].mean(axis = 1)
    df2 = df2.sort_values('總平均', ascending = False)
    df2['名次'] = list(range(1,len(df2)+1))
    
    print(df2)
    df2.to_csv('Grades1.csv',index = False)
    
DFOfGrades()