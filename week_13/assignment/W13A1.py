import pandas as pd


def DateOfBus():
    table = pd.read_html('https://www.ubus.com.tw/Booking/FareInquiry')
    df = table[0]
    df.columns = ["路線名稱","優惠時段","原價時段","半票票價","軍優票價","去回票價"]
    print(df)
    df.to_csv('bus.csv')